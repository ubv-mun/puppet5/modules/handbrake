# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include handbrake::install
class handbrake::install {
  package { $handbrake::package_name:
    ensure => $handbrake::package_ensure,
  }
}
